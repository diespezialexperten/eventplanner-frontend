{ withAndroidSDK ? false, withXCodeEnv ? false }:

with import <nixpkgs> {}; {
  env = stdenv.mkDerivation {
    name = "eventplanner-dev";
    src = null;

    buildInputs = [ nodejs-8_x nodePackages.react-native-cli yarn watchman ]
      ++ lib.optional withAndroidSDK androidsdk
      ++ lib.optional (withXCodeEnv && stdenv.isDarwin) xcodeenv;

    shellHook = ''
      yarn run debug
    '';
  };
}
