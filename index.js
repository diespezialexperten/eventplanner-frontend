/**
 * @flow
 */

import App  from './src/app';
import Expo from 'expo'

// Disable the warning log feature:
console.disableYellowBox = process.env.NODE_ENV == 'production';
Expo.registerRootComponent(App)
