const initialState = {
  baseUser: {
    user: {}
  },
  authError: {},
  user: {},
  justSignedUp: false,
};

export default (state = initialState, action) => {
  if (action.type === '@@jalolo/LOGIN_REQUEST_SUCCESS') {
    const newState = {
      user: action.payload,
    };

    return Object.assign({}, state, newState);
  }

  if (action.type === '@@jalolo/LOGIN_REQUEST_FAILURE') {
    const newState = {
      authError: action.payload,
    };

    return Object.assign({}, state, newState);
  }

  if (action.type === '@@jalolo/SIGNUP_REQUEST_SUCCESS') {
    // todo: baseUser will have a falsy population
    const newState = {
      baseUser: action.payload,
      justSignedUp: true,
    };

    return Object.assign({}, state, newState);
  }

  if (action.type === '@@jalolo/SIGNUP_REQUEST_FAILURE') {
    const newState = {
      authError: action.payload,
    };

    return Object.assign({}, state, newState);
  }

  if (action.type === '@@jalolo/LOAD_USER_INFORMATION_SUCCESS') {
    const newState = {
      baseUser: {
        user: action.payload,
      }
    };

    return Object.assign({}, state, newState);
  }

  if (action.type === '@@jalolo/LOAD_USER_INFORMATION_FAILURE') {
    const newState = {
      authError: action.payload,
    };

    return Object.assign({}, state, newState);
  }

  return state;
};
