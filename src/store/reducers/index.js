import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import create from '../../containers/create';
import planned from '../../containers/planned';
import settings from '../../containers/settings';
import auth from '../../containers/auth';

import createReducer from './create.reducer';
import plannedReducer from './planned.reducer';
import settingsReducer from './settings.reducer';
import authReducer from './auth.reducer';

export default combineReducers({
  [create.NAME]: createReducer,
  [planned.NAME]: plannedReducer,
  [settings.NAME]: settingsReducer,
  [auth.NAME]: authReducer,
  form: formReducer,
});
