import { GET_LIST_OF_PLANNED_EVENTS } from '../../containers/planned/planned.const';

const initialState = [
  {
    event: {
      title: '',
      shortId: 'h4Vlsj23',
      description: '',
      location: ['48.156757', '11.595729'],
      locationImage: 'https://picsum.photos/200/300/?random',
      locationInWords: '',
      invitees: ['user1', 'user2', 'user3'],
      accepted: [''],
    },
  },
];

export default (state = initialState, action) => {

  if (action.type === GET_LIST_OF_PLANNED_EVENTS) {
    const newState = {
      eventList: action.payload,
    };

    return Object.assign({}, state, newState);
  }

  return state;
};

