import { createStore, applyMiddleware, compose } from 'redux';
import axios from 'axios';
import thunk from 'redux-thunk';
import axiosMiddleware from 'redux-axios-middleware';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'remote-redux-devtools';

import config from '../util/config/default';
import sagas from './sagas';
import reducers from './reducers';

const axiosClient = axios.create({
  baseURL: config.api.baseURL,
  responseType: config.api.responseType,
});

const axiosMiddlewareConfig = {
  interceptors: {
    request: [
      ({ getState, getSourceAction }, req) => {
        // check if token exists already
        if (getState().token.length > 0) {
          // add Authorization header if token is set
          req.headers.Authorization = getState().token;
        }
        return req;
      },
    ],
    response: [
      {
        success: ({ getState, dispatch, getSourceAction }, res) => res,
        error: ({ getState, dispatch, getSourceAction }, error) => {
          // if the client is not authorized fetch the access token
          if (error.response.status === 401 && error.config && !error.config.isRetryRequest) {
            // reset the access token
            dispatch({
              type: 'RESET_TOKEN',
            });
          }
        },
      },
    ],
  },
};

const sagaMiddleware = createSagaMiddleware();

/* eslint-disable no-underscore-dangle */
const composeEnhancers = composeWithDevTools({ realtime: true, port: 8000, suppressConnectErrors: false });
/* eslint-enable */

/* eslint-enable */
const middlewares = [axiosMiddleware(axiosClient, axiosMiddlewareConfig), sagaMiddleware, thunk];

const store = createStore(reducers, composeEnhancers(applyMiddleware(...middlewares)));

sagaMiddleware.run(sagas);

export default store;
