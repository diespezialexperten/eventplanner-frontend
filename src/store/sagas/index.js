import {put, takeEvery, call, take} from 'redux-saga/effects';
import firebase from 'firebase';
import ReduxSagaFirebase from 'redux-saga-firebase'
import {
  signUpUserFailure,
  signUpUserSuccess,
  loginSuccess,
  loginFailure,
  signOutSuccess,
  signOutFailure
} from '../../containers/auth/auth.action';

import {
  userIsNotAuthenticated,
  loadUserInformationFailure,
  loadUserInformationSuccess
} from '../../containers/startPage/startPage.action';

const myFirebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyB2U6HL0akJLZYJdAmS3VFYY1u74t9G6t8",
  authDomain: "esmb-e9648.firebaseapp.com",
  databaseURL: "https://esmb-e9648.firebaseio.com",
  projectId: "esmb-e9648",
  storageBucket: "esmb-e9648.appspot.com",
  messagingSenderId: "476305613322"
});

const reduxSagaFirebase = new ReduxSagaFirebase(myFirebaseApp);

const emailAuthProvider = new firebase.auth.EmailAuthProvider();

function* checkForUserAuthentificationSaga() {
  const channel = yield call(reduxSagaFirebase.auth.channel);

  while (true) {
    const {error, user} = yield take(channel);

    if (user) yield put(loginSuccess(user));
    else yield put(userIsNotAuthenticated(error));
  }
}

function* loadUserInformationSaga(action) {
  const {uid} = action.payload;

  const userData = yield call(reduxSagaFirebase.database.read, 'users/' + uid);
  const userKey = Object.keys(userData)[0];

  yield put(loadUserInformationSuccess(userData[userKey]));
}

function* createUserSaga(action) {
  const {email, password, fname} = action.payload;

  try {
    const user = yield call(reduxSagaFirebase.auth.createUserWithEmailAndPassword, email, password);
    // todo: investigate why? When getting the user there must be smth going wrong!
    const newUser = {
      user: {
        fname: fname,
        email: email,
      }
    };

    const key = yield call(reduxSagaFirebase.database.create, 'users/' + user.uid, {
      user: {
        fname: fname,
        email: email,
      }
    });


    yield put(signUpUserSuccess(newUser));
  }
  catch (error) {
    console.log(error);
    yield put(signUpUserFailure(error));
  }
}

function* loginSaga(action) {
  const {email, password} = action.payload;
  try {
    const data = yield call(reduxSagaFirebase.auth.signInWithEmailAndPassword, email, password);
    yield put(loginSuccess(data))
  }
  catch (error) {
    yield put(loginFailure(error))
  }
}

function* fbLoginSaga(action) {
  const token = action.payload;
  const credential = firebase.auth.FacebookAuthProvider.credential(token);

  firebase.auth().signInWithCredential(credential).then((result) => {
    console.log(result);
    // todo: here a check needs to be implemented to save the name that is returned by facebook if the user is signing up: diff between oauth signup and signin!
    loginSuccess(result)
  }, (error) => {
    loginFailure(error)
  })
}

function* goglLoginSaga(action) {
  const token = action.payload;
  const credential = firebase.auth.GoogleAuthProvider.credential(token);
  firebase.auth().signInWithCredential(credential).then((result) => {
    loginSuccess(result)
  }, (error) => {
    console.log(error);
    loginFailure(error)
  })
}

function* signOutSaga() {
  try {
    const data = yield call(reduxSagaFirebase.auth.signOut);
    yield put(signOutSuccess(data));
  }
  catch (error) {
    yield put(signOutFailure(error));
  }
}

export default function* rootSaga() {
  yield [
    takeEvery('@@jalolo/GET_USER_AUTH_STATUS', checkForUserAuthentificationSaga),
    takeEvery('@@jalolo/LOAD_USER_INFORMATION', loadUserInformationSaga),
    takeEvery('@@jalolo/LOGIN_REQUEST', loginSaga),
    takeEvery('@@jalolo/LOGIN_FACEBOOK_REQUEST', fbLoginSaga),
    takeEvery('@@jalolo/LOGIN_GOOGLE_REQUEST', goglLoginSaga),
    takeEvery('@@jalolo/SIGNOUT_REQUEST', signOutSaga),
    takeEvery('@@jalolo/SIGNUP_REQUEST', createUserSaga)
  ]
}