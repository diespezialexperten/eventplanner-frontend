/**
 * @flow
 */

import React, { Component } from 'react'
import { Provider }         from 'react-redux'
import StartPage            from './containers/startPage'
import store                from './store/store'
import { Font, AppLoading } from 'expo'
import { Text }             from 'react-native'
import "@expo/vector-icons"

/**
 * Bootstrapping component to preload fonts in a lifecycle hook and
 * bootstraps the application with Redux.
 *
 * @author Maximilian Bosch <maximilian@mbosch.me>
 */
export default class App extends Component {
  state = {
    fontReady: false
  }

  async componentWillMount() {
    Font.loadAsync({
      awesome: require('../node_modules/react-native-vector-icons/Fonts/FontAwesome.ttf')
    })
    this.setState({ fontReady: true })
  }

  render() {
    return (
      <Provider store={store}>
        {this.state.fontReady ? <StartPage /> : <AppLoading />}
      </Provider>
    );
  }
}
