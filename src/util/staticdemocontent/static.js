//very basic. A lot is still missing
export const staticContent = [
  {
    title: 'Chillen am Eisbach',
    shortId: 'h4Vlsj23',
    description: 'Beatae cum impedit laboriosam magnam placeat qui tenetur. Eaque, maiores, nobis?',
    location: ['48.156757', '11.595729'],
    locationImage: 'https://picsum.photos/200/300/?random',
    locationInWords: 'Eisbach',
    invitees: ['user1', 'user2', 'user3'],
    accepted: ['user2'],
  },
  {
    title: 'Grillen beim Tony',
    shortId: '23l0m4l5',
    description: 'Beatae cum impedit laboriosam magnam placeat qui tenetur. Eaque, maiores, nobis?',
    location: ['48.156757', '11.595729'],
    locationImage: 'https://picsum.photos/200/300/?random',
    locationInWords: 'Oberfoehringerstrasse',
    invitees: ['user1', 'user2', 'user3'],
    accepted: [''],
  },
  {
    title: 'Club',
    shortId: '39mJh50',
    description: 'Beatae cum impedit laboriosam magnam placeat qui tenetur. Eaque, maiores, nobis?',
    location: ['48.156757', '11.595729'],
    locationImage: 'https://picsum.photos/200/300/?random',
    locationInWords: 'Mint',
    invitees: ['user1', 'user2', 'user3'],
    accepted: ['user2', 'user1', 'user3'],
  },
];