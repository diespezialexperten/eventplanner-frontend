import React from 'react';
import { PropTypes } from 'prop-types';
import { TabNavigator, StackNavigator, NavigationActions } from 'react-navigation';
import {Icon} from 'react-native-elements';

import CreateEvent from '../../containers/create/CreateEvent';
import Planned from '../../containers/planned/Planned';
import Detail from '../../containers/planned/Detail';
import Settings from '../../containers/settings/Settings';

// Make sure the navigation action is only dispatched once.
const navigateOnce = (getStateForAction) => (action, state) => {
  const { type, routeName } = action;
  return (
    state &&
    type === NavigationActions.NAVIGATE &&
    routeName === state.routes[state.routes.length - 1].routeName
  ) ? state : getStateForAction(action, state);
};

const PlannedStack = StackNavigator({
  planned: {
    screen: Planned,
    navigationOptions: {
      headerTitle: 'Planned',
      tabBarLabel: 'Planned',
      tabBarIcon: ({ color = '#494644' }) => <Icon name="list" size={35} color={color} />,
    },
  },
  detail: {
    screen: Detail,
    navigationOptions: ({ navigation }) => ({
      title: `Detail`,
      tabBarIcon: ({ color = '#494644' }) => <Icon name="list" size={35} color={color} />,
    }),
  }
});

PlannedStack.router.getStateForAction = navigateOnce(PlannedStack.router.getStateForAction);

const CreateStack = StackNavigator({
  create: {
    screen: CreateEvent,
    navigationOptions: {
      headerTitle: 'Create',
      tabBarLabel: 'Create',
      tabBarIcon: ({ color = '#494644' }) => <Icon name="create" size={35} color={color} />,
    },
  },
});

CreateStack.router.getStateForAction = navigateOnce(CreateStack.router.getStateForAction);

const SettingsStack = StackNavigator({
  settings: {
    screen: Settings,
    navigationOptions: {
      headerTitle: 'Settings',
      tabBarLabel: 'Settings',
      tabBarIcon: ({ color = '#494644' }) => <Icon name="settings" size={35} color={color} />,
    },
  },
});

SettingsStack.router.getStateForAction = navigateOnce(SettingsStack.router.getStateForAction);

export default TabNavigator({
  create: {
    screen: CreateStack,
  },
  planned: {
    screen: PlannedStack,
  },
  settings: {
    screen: SettingsStack,
  },
}, {
  initialRouteName: 'planned',
  swipeEnabled: true,
  animationEnabled: false,
  tabBarPosition: 'bottom',
});
