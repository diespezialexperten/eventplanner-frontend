/**
 * @flow
 */

import React from 'react';
import { PropTypes } from 'prop-types';
import { View, Text } from 'react-native';
import { TabNavigator, StackNavigator } from 'react-navigation';

import { navigateOnce } from '../other/navigation/navigateOnce';
import SignIn from '../../containers/auth/SignIn';
import SignUp from '../../containers/auth/SignUp';

const Signin = StackNavigator({
  login: {
    screen: SignIn,
    navigationOptions: {
      header: null,
      tabBarVisible: false,
    },
  },
});

const Signup = StackNavigator({
  signup: {
    screen: SignUp,
    navigationOptions: () => ({
      header: null,
      tabBarVisible: false,
    }),
  }
});


Signup.router.getStateForAction = navigateOnce(Signup.router.getStateForAction);
Signin.router.getStateForAction = navigateOnce(Signin.router.getStateForAction);

export default TabNavigator({
  signup: {
    screen: Signup,
  },
  login: {
    screen: Signin,
  },
}, {
  initialRouteName: 'signup',
  swipeEnabled: true,
  animationEnabled: false,
  tabBarPosition: 'bottom',
});
