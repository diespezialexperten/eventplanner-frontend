/*
 * The license to this project can be found in the project root.
 *
 * Benjamin Bieler <ben@benbieler.com>
 * Maximilian Bosch <maximilian@mbosch.me>
 */

import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export const Root = () => <View style={styles.container}>
    <Text style={styles.welcome}>It works!!!</Text>
</View>;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});