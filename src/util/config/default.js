const config = {
  api: {
    baseUrl: 'http://localhost:8081',
    returnType: 'json',
  },
};

export default config;