import React, {Component} from 'react';
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
  View
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import {authform} from '../../../styles/forms';

export default class SignupForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "benjaminbieler2014@gmail.com",
      password: "lorem123",
      fname: "Ben",
      emailError: false,
      passwordError: false,
      fnameError: false,
    }
  }

  validateSignup = () => {
    const emailError = !this.state.email.length;;
    const passwordError = !this.state.password.length;
    fnameError = !this.state.fname.length;

    this.setState({ fnameError, emailError, passwordError });

    if (emailError === false && passwordError === false && fnameError === false) {
      this.props.createUser(this.state.email, this.state.password, this.state.fname)
    }
  };

  loadEmailPasswordForm() {
    return (
      <View style={authform.container}>
        <View style={authform.inputContainer}>
          <TextInput
            placeholder="Email"
            placeholderTextColor="rgb(255,255,255)"
            returnKeyType="next"
            onSubmitEditing={() => this.passwordInput.focus()}
            style={[authform.input, this.state.emailError && authform.inputError]}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={(text) => this.setState({ email: text.trim() })}
          />
          <TextInput
            placeholder="Password"
            placeholderTextColor="rgb(255,255,255)"
            secureTextEntry
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={false}
            style={[authform.input, this.state.passwordError && authform.inputError]}
            ref={(input) => this.passwordInput = input}
            onChangeText={(text) => this.setState({ password: text.trim() })}
          />
          <TextInput
            placeholder="First name"
            placeholderTextColor="rgb(255,255,255)"
            returnKeyType="next"
            style={[authform.input, this.state.fnameError && authform.inputError]}
            autoCapitalize="words"
            autoCorrect
            onChangeText={(text) => this.setState({ fname: text.trim() })}
          />
        </View>
        <View style={authform.buttonContainer}>
          <TouchableOpacity onPress={() => this.validateSignup()}>
            <Text style={authform.button}>SIGN UP</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    if (this.props.authenticating) {
      return <Spinner visible animation="fade" textContent={"Loading..."} textStyle={{color: '#FFF'}}/>
    }
    return this.loadEmailPasswordForm()
  }
}
