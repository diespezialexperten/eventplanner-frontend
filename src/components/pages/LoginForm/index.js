import React from 'react';
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
  View
} from 'react-native';

import { authform } from '../../../styles/forms';

export default class LoginForm extends React.Component {

  constructor(props) {
    super(props);
    // save the username in state so that you can just press the signup button:
    this.state = { email: "benjaminbieler2014@gmail.com", password: "lorem123", emailError: false, passwordError: false }
  }

  validateLogin() {
    const emailError = !this.state.email.length
    const passwordError = !this.state.password.length
    this.setState({ emailError, passwordError });

    if (emailError === false && passwordError === false) {
      this.props.loginUser(this.state.email, this.state.password)
    }
  }

  render() {
    return (
      <View style={authform.container}>
        <View style={authform.inputContainer}>
          <TextInput
            placeholder="Email"
            placeholderTextColor="rgb(255,255,255)"
            returnKeyType="next"
            onSubmitEditing={() => this.passwordInput.focus()}
            style={[authform.input, this.state.emailError && authform.inputError]}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={(text) => this.setState({ email: text.trim() })}
          />
          <TextInput
            placeholder="Password"
            placeholderTextColor="rgb(255,255,255)"
            secureTextEntry
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={false}
            style={[authform.input, this.state.passwordError && authform.inputError]}
            ref={(input) => this.passwordInput = input}
            onChangeText={(text) => this.setState({ password: text.trim() })}
          />
        </View>
        <View style={authform.buttonContainer}>
          <TouchableOpacity onPress={() => this.validateLogin()}>
            <Text style={authform.button}>LOG IN</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
