import { StyleSheet } from 'react-native';

export const authform = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputContainer: {
    flex: 3,
    flexDirection: 'column',
    paddingLeft: 20,
    paddingRight: 20,
  },
  input: {
    height: 50,
    backgroundColor: 'rgba(130,130,130, 0.7)',
    marginBottom: 15,
    color: '#ffffff',
    paddingHorizontal: '8%',
    borderTopColor: '#fff',
    borderRightColor: '#fff',
    borderLeftColor: '#fff',
    borderBottomColor: '#eaeaea',
    borderWidth: 1.0,
  },
  inputError: {
    borderColor: '#ff0000',
  },
  buttonContainer: {
    backgroundColor: 'rgba(55,93,124,0.6)',
    borderColor: 'rgba(55,93,124,0.6)',
    paddingVertical: 15,
    height: 50,
    marginBottom: 50,
    paddingRight: '15%',
    paddingLeft: '15%',
    justifyContent: 'center',
  },
  button: {
    textAlign: 'center',
    borderColor: 'rgba(55,93,124,0.6)',
    color: '#fff',
    fontWeight: '400',
    fontSize: 17,
  },
});
