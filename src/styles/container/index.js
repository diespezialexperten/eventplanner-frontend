import {StyleSheet} from 'react-native';

export const authContainer = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: null,
    height: null
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  formContainer: {
    flex: 2,
  },
  headerContainer: {
    flex: 1,
    marginTop: 0,
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    justifyContent: 'center',
  },
  socialContainer: {
    flex: 1.3,
    marginTop: 25,
    flexDirection: 'column',
  },
  tabContainer: {
    flex: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 1,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  title: {
    marginTop: 55,
    width: 400,
    height: 200,
  },
  redirectLink: {
    color: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    textAlign: 'center',
    marginBottom: 20,
  },
  centerText: {
    color: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    textAlign: 'center',
    marginBottom: 20,
  }
});
