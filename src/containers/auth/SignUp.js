/**
 * @flow
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  Alert,
  Button,
  KeyboardAvoidingView,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  ScrollView
} from 'react-native';
import { SocialIcon } from 'react-native-elements'

import SignupForm from '../../components/pages/SignupForm';
import * as actions from './auth.action'

import {facebook} from './oauth/facebook';
import {google} from './oauth/google';

import {authContainer} from '../../styles/container';

/**
 * SignUp Container
 *
 * @author Benjamin Bieler <ben@benbieler.com>
 */
class SignUp extends React.Component {

  state = {
    progress: false,
  };

  createUser = (email, password, fname) => {
    //todo: fix this authenticator!
    this.setState({progress: false});
    this.props.actions.signUpUser(email, password, fname);
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <KeyboardAvoidingView style={authContainer.container}>
        <ImageBackground source={require('../../styles/images/beachparty.jpg')} style={authContainer.backgroundImage}>
          <View style={authContainer.headerContainer}>
            <Image
              style={authContainer.title}
              source={require('../../styles/images/Logo.png')}
            />
          </View>
          <View style={authContainer.socialContainer}>
            <SocialIcon
              title='Sign In With Facebook'
              button
              onPress={() => facebook(this.props.actions.fbLogin)}
              type='facebook'
            />
            <SocialIcon
              title='Sign In With Google'
              button
              onPress={() => google(this.props.actions.goglLogin)}
              type='google-plus-official'
            />
          </View>
          <View style={authContainer.formContainer}>
            <SignupForm createUser={this.createUser} addFname={this.addFname}
                        progress={this.state.progress}/>
          </View>
          <View>
            <Text style={authContainer.redirectLink} onPress={() => { navigate('login', { swipeEnabled: true }) }}>Already have an account? Login now.</Text>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
