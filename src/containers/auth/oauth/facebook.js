import {Alert} from 'react-native';
import { Facebook } from 'expo'

export const facebook = async (handleFirebaseAuth) => {
  const { type, token } = await Facebook.logInWithReadPermissionsAsync(
    "2042700415948840",
    { permissions: ['public_profile', 'user_about_me', 'email'] }
  )
  if (type == "success") {
    handleFirebaseAuth(token)
  } else {
    Alert.alert("Authentication failed!")
  }
};
