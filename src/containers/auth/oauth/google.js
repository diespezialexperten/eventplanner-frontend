import {Alert} from 'react-native';
import { Google } from 'expo'

export const google = (handleFirebaseAuth) => {
  Google.logInAsync({
    iosClientId: '476305613322-gms2rhc3tliocoprt8gooc0lmlsh10ci.apps.googleusercontent.com',
    scopes: ['profile', 'email']
  })
    .then(user => handleFirebaseAuth(user.idToken))
    .catch(err => Alert.error('Authentication with the Google service failed!'))
}
