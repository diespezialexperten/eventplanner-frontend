//////////////////////////////////////////////////////////////
///////////// Email - Password Authentification //////////////
//////////////////////////////////////////////////////////////
export const signUpUser = (email, password, fname) => (dispatch) => {
  dispatch({type: '@@jalolo/SIGNUP_REQUEST', payload: {email: email, password: password, fname: fname,}})
};
export const loginUser = (email, password) => (dispatch) => {
  dispatch({type: '@@jalolo/LOGIN_REQUEST', payload: {email: email, password: password,}})
};
export const signUpUserSuccess = (user) => (dispatch) => {
  dispatch({type: '@@jalolo/SIGNUP_REQUEST_SUCCESS', payload: user})
};
export const signUpUserFailure = (error) => (dispatch) => {
  dispatch({type: '@@jalolo/SIGNUP_REQUEST_FAILURE', payload: error})
};
export const loginSuccess = (data) => (dispatch) => {
  dispatch({type: '@@jalolo/LOGIN_REQUEST_SUCCESS', payload: data})
};
export const loginFailure = (error) => (dispatch) => {
  dispatch({type: '@@jalolo/LOGIN_REQUEST_FAILURE', payload: error})
};

//////////////////////////////////////////////////////////////
///////////////// Google Authentification ////////////////////
//////////////////////////////////////////////////////////////
export const goglLogin = (credential) => (dispatch) => {
  dispatch({type: '@@jalolo/LOGIN_GOOGLE_REQUEST', payload: credential})
};

//////////////////////////////////////////////////////////////
///////////////// Facebook Authentification //////////////////
//////////////////////////////////////////////////////////////
export const fbLogin = (credential) => (dispatch) => {
  dispatch({type: '@@jalolo/LOGIN_FACEBOOK_REQUEST', payload: credential})
};

//////////////////////////////////////////////////////////////
//////////////////////// SignOut /////////////////////////////
//////////////////////////////////////////////////////////////
export const signOutRequest = () => (dispatch) => {
  dispatch({type: '@@jalolo/SIGNOUT_REQUEST'})
};
export const signOutSuccess = () => (dispatch) => {
  dispatch({type: '@@jalolo/SIGNOUT_SUCCESS'})
};
export const signOutFailure = () => (dispatch) => {
  dispatch({type: '@@jalolo/SIGNOUT_FAILURE'})
};