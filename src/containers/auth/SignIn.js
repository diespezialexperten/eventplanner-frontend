/**
 * @flow
 */

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
  Image,
  ImageBackground,
  Text,
  TouchableHighlight,
  View,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import { SocialIcon } from 'react-native-elements';

import LoginForm from '../../components/pages/LoginForm';
import * as actions from './auth.action'

import {facebook} from './oauth/facebook';
import {google} from './oauth/google';
import { authContainer } from '../../styles/container';

/**
 * SignIn Container
 *
 * @author Benjamin Bieler <ben@benbieler.com>
 */
class SignIn extends React.Component {

  constructor() {
    super();
    this.state = {
      authenticating: false,
    }
  }

  loginUser = (email, password) => {
    //todo: fix the authenticator spinner!
    this.setState({authenticating: false});
    this.props.actions.loginUser(email, password);
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <KeyboardAvoidingView style={authContainer.container}>
        <ImageBackground source={require('../../styles/images/beachparty.jpg')} style={authContainer.backgroundImage}>
          <View style={authContainer.headerContainer}>
            <Image
              style={authContainer.title}
              source={require('../../styles/images/Logo.png')}
            />
          </View>
          <View style={authContainer.socialContainer}>
            <SocialIcon
              title='Sign In With Facebook'
              button
              onPress={() => facebook(this.props.actions.fbLogin)}
              type='facebook'
            />
            <SocialIcon
              title='Sign In With Google'
              button
              onPress={() => google(this.props.actions.goglLogin)}
              type='google-plus-official'
            />
          </View>
          <ScrollView style={authContainer.formContainer}>
            <LoginForm loginUser={this.loginUser} authenticating={this.state.authenticating}/>
          </ScrollView>
          <View>
            <Text style={authContainer.redirectLink} onPress={() => { navigate('signup') }}>Do not have an account yet? Sign up now.</Text>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
