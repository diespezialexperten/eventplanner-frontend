import SignUp from './SignUp';
import SignIn from './SignIn';

const NAME = 'auth';

export default {
  NAME,
  SignUp,
  SignIn,
};
