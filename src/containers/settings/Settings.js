import React from 'react';
import {PropTypes} from 'prop-types';
import {ScrollView, KeyboardAvoidingView, Text, TouchableOpacity, View, StyleSheet, Image, Alert} from 'react-native';
import {connect} from 'react-redux';
import Modal from 'react-native-modal'
import {Icon, Card, List, ListItem, Button, ButtonGroup} from 'react-native-elements';

import {bindActionCreators} from 'redux';
import * as settingsActions from './settings.action';
import * as authActions from '../auth/auth.action';


class Settings extends React.Component {
  state = {
    isModalVisible: false,
    selectedIndex: 0,
  };

  renderButton = (text, onPress) => (
    <Icon name="settings" onPress={onPress}/>
  );

  handleSignOut = () => {
    Alert.alert('Warning', 'Are you sure that you want to log out?', [
      {text: 'No', style: 'cancel'},
      {text: 'Yes', onPress: () => this.props.actions.auth.signOutRequest()},
    ])
  };

  renderModalContent = () => (
    <View>
      <Card title="Settings">
        <View>
          <Text style={{padding: 15}} onPress={() => {console.log('notifications')}}>Notifications</Text>
          <Text style={{padding: 15}} onPress={() => {console.log('account')}}>Account</Text>
          <Text style={{padding: 15}} onPress={() => {console.log('Help')}}>Help</Text>
          <Text style={{padding: 15}} onPress={() => {console.log('tell a friend')}}>Tell a friend</Text>
          <Text style={{padding: 15}} onPress={() => this.handleSignOut()}>Sign out</Text>
        </View>
      </Card>
    </View>
  );

  render() {
    return (
      <KeyboardAvoidingView behavior="position">
        <ScrollView key={1} style={{backgroundColor: 'white'}}>
          <View>
            {this.renderButton('Settings', () =>
              this.setState({visibleModal: 6}),
            )}
            <Modal
              isVisible={this.state.visibleModal === 6}
              animationIn={'zoomIn'}
              animationOut={'zoomOut'}
              animationInTiming={500}
              animationOutTiming={500}
              backdropTransitionInTiming={200}
              backdropTransitionOutTiming={600}
              onBackdropPress={() => this.setState({ visibleModal: null })}>
              { this.renderModalContent() }
            </Modal>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    )
  }
}

Settings.propTypes = {
  actions: PropTypes.object.isRequired,
  baseUser: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  return {
    baseUser: state.auth.baseUser.user.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      auth: bindActionCreators(authActions, dispatch),
      get_current_user_settings: bindActionCreators(settingsActions, dispatch)
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);