import * as actions from './settings.action';
import { NAME } from './settings.const';
import Settings from './Settings';

export default {
  actions,
  NAME,
  Settings,
};
