/**
 * @flow
 */

import React from 'react';
import {bindActionCreators} from 'redux'
import {Alert, Text} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from './startPage.action.js'
import Authentication from '../../util/routing/nonAuthRouting';
import Root from '../../util/routing';

function StartPage({actions, auth}) {
  actions.checkIfUserIsAuthenticated();

  if (Object.keys(auth.user).length !== 0) {
      return <Root />
  } else {
    if (Object.keys(auth.authError).length !== 0) {
      Alert.alert(auth.authError.message)
    }

    return <Authentication />
  }
}

StartPage.propTypes = {
  auth: PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StartPage);
