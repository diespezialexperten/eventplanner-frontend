/**
 * @flow
 */

export const checkIfUserIsAuthenticated = () => (dispatch) => {
  dispatch({
    type: '@@jalolo/GET_USER_AUTH_STATUS'
  })
};

export const userIsNotAuthenticated = () => (dispatch) => {
  dispatch({
    type: '@@jalolo/USER_IS_NOT_LOGGED_IN'
  })
};

export const loadUserData = (user) => (dispatch) => {
  dispatch({
    type: '@@jalolo/LOAD_USER_INFORMATION',
    payload: user
  })
};

export const loadUserInformationSuccess = (data) => (dispatch) => {
  dispatch({
    type: '@@jalolo/LOAD_USER_INFORMATION_SUCCESS',
    payload: data
  })
};

export const loadUserInformationFailure = (data) => (dispatch) => {
  dispatch({
    type: '@@jalolo/LOAD_USER_INFORMATION_FAILURE',
    payload: data
  })
};

