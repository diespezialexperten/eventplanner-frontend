import React from 'react';
import {PropTypes} from 'prop-types';
import {List, ListItem, Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {Dimensions, ScrollView} from 'react-native'
import {staticContent} from '../../util/staticdemocontent/static';

/**
 * Main view component.
 *
 * @author Ben Bieler <ben@benbieler.com>
 * @returns {React.Component}
 */
class Planned extends React.Component {
  navigateToDetailsView(element) {
    this.props.navigation.navigate('detail', {...element});
  }

    renderList = (events) => {
      if (events.length > 1) {
        return <List>
          {events.map(event => (
            <ListItem
              roundAvatar
              avatar={event.locationImage}
              title={event.title}
              onPress={() => { this.navigateToDetailsView(event); }}
            />
          ))}
        </List>
      }

      return <View>
        <Icon
          name="filter-9-plus"
          onPress={() => this.props.actions.loadContactsRequest(this.props.uid)}
        />
        <Text>No events</Text>
      </View>
    };

    render() {
      // const events = this.props.events;

      return (
        <ScrollView>
          {this.renderList(staticContent) }
        </ScrollView >
      );
    }
}

Planned.propTypes = {
  planned: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  planned: state.planned,
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Planned);
