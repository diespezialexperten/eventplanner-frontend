import React from 'react';
import { PropTypes } from 'prop-types';
import {ScrollView} from 'react-native';
import {Tile} from 'react-native-elements';

/**
 * Detail view component.
 *
 * @author Ben Bieler <ben@benbieler.com>
 * @returns {React.Component}
 */
export default class Detail extends React.Component {
  render() {
    const { locationImage, title, description } = this.props.navigation.state.params;
    return (
      <ScrollView>
          <Tile
            imageSrc={{uri: locationImage}}
            featured
            caption={description}
            title={title.toUpperCase()}
          />
      </ScrollView>
      );
  }
}
