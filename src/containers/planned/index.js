import * as actions from './planned.action';
import { NAME, GET_LIST_OF_PLANNED_EVENTS } from './planned.const';
import Detail from './Detail';
import Planned from './Planned';

export default {
  actions,
  NAME,
  GET_LIST_OF_PLANNED_EVENTS,
  Detail,
  Planned,
};
