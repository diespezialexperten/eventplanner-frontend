import React from 'react';
import {Button} from 'react-native-elements';
import {View, Text} from 'react-native'
import {PropTypes} from 'prop-types';

/**
 * Main view component.
 *
 * @author Ben Bieler <ben@benbieler.com>
 * @returns {React.Component}
 */
export default class CreateEvent extends React.Component {
  render() {
    return (
      <View>
        <Button
          onPress={() => {console.log('lorem')}}
          title='NEW EVENT'
        />
      </View>
    );
  }
}