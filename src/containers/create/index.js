import * as actions from './create.action';
import { NAME } from './create.const';
import CreateEvent from './CreateEvent';

export default {
  actions,
  NAME,
  CreateEvent,
};
