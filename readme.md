# Eisbach Session Manager

Simple application to make organizing Augustiner-based sessions on Munich's Eisbach easier.

## Prerequisites

- [NodeJS](https://nodejs.org/en/download/) (at least v8.x)
- [Yarn](https://yarnpkg.com/en/) (at least v1.5)
- [React Native Debugger](https://github.com/jhen0409/react-native-debugger)
- (optional) [Nix](https://nixos.org/nix/) (at least v2.0)
- TCP ports `8081`, `8000` and `19000` opened

## Development

- Open a development shell using Nix's [`nix-shell`](https://nixos.org/nixos/nix-pills/developing-with-nix-shell.html) feature:

        nix-shell

- Setup and run:

        yarn
        yarn run start

## Debugging

Open the `React Native Debugger` and run the following commands after that:

```
yarn run debug
yarn run start
```

## Deployment

The applications can be built for native targets with Expo:

```
yarn run build:ios
yarn run build:android
```

Please keep in mind that a [Darwin kernel](https://github.com/apple/darwin-xnu) is needed for iOS builds!
